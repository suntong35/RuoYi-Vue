package com.ruoyi.test0.test01.service;

import java.util.List;
import com.ruoyi.test0.test01.domain.SysStudentDept;
import com.baomidou.mybatisplus.extension.service.IService;
import com.ruoyi.test0.test01.domain.SysStudentDeptVo;

/**
 * 测试用01Service接口
 * 
 * @author ruoyi
 * @date 2022-05-06
 */
public interface ISysStudentDeptService extends IService<SysStudentDept>
{
    /**
     * 查询测试用01列表
     *
     * @param sysStudentDept 测试用01
     * @return 测试用01集合
     */
    public List<SysStudentDept> selectSysStudentDeptList(SysStudentDept sysStudentDept);

    public SysStudentDeptVo getOneSysStudentDept(Long id);

    /**
     * 查询测试用01
     * 
     * @param id 测试用01主键
     * @return 测试用01
     */
    public SysStudentDept selectSysStudentDeptById(Long id);



    /**
     * 新增测试用01
     * 
     * @param sysStudentDept 测试用01
     * @return 结果
     */
    public int insertSysStudentDept(SysStudentDept sysStudentDept);

    /**
     * 修改测试用01
     * 
     * @param sysStudentDept 测试用01
     * @return 结果
     */
    public int updateSysStudentDept(SysStudentDept sysStudentDept);

    /**
     * 批量删除测试用01
     * 
     * @param ids 需要删除的测试用01主键集合
     * @return 结果
     */
    public int deleteSysStudentDeptByIds(Long[] ids);

    /**
     * 删除测试用01信息
     * 
     * @param id 测试用01主键
     * @return 结果
     */
    public int deleteSysStudentDeptById(Long id);
}
