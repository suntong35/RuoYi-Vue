package com.ruoyi.test0.test01.domain;

import java.util.List;
import java.util.Optional;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.*;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 测试用01对象 sys_student_dept
 * 
 * @author ruoyi
 * @date 2022-05-06
 */
@Data
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
@ToString(callSuper = true)
@TableName("sys_student_dept")
public class SysStudentDept extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** $column.columnComment */
    private Long id;

    /** 部门名称 */
    @Excel(name = "部门名称")
    private String deptName;

    /** 学生信息信息 */
    @TableField(exist = false)
    private List<SysStudent> sysStudentList;


    public SysStudentDept(SysStudentDept sysStudentDept) {
        Optional.ofNullable(sysStudentDept).ifPresent(e -> {
            this.id = e.getId();
            this.deptName = e.getDeptName();
        });
    }

}
