package com.ruoyi.test0.test01.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;

import com.ruoyi.test0.test01.domain.SysStudentDeptVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.test0.test01.domain.SysStudentDept;
import com.ruoyi.test0.test01.service.ISysStudentDeptService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 测试用01Controller
 * 
 * @author ruoyi
 * @date 2022-05-06
 */
@RestController
@RequestMapping("/test0/test01")
@Api(tags = "测试用01")
public class SysStudentDeptController extends BaseController
{
    @Autowired
    private ISysStudentDeptService sysStudentDeptService;

    /**
     * 查询测试用01列表
     */
    @PreAuthorize("@ss.hasPermi('test0:test01:list')")
    @ApiOperation("获取测试用01列表")
    @GetMapping("/list")
    public TableDataInfo list(SysStudentDept sysStudentDept)
    {
        startPage();
        List<SysStudentDept> list = sysStudentDeptService.selectSysStudentDeptList(sysStudentDept);
        TableDataInfo dataTable = getDataTable(list);
        return dataTable;
    }

    @ApiOperation("获取测试用01列表")
    @GetMapping("/list1")
    public SysStudentDeptVo getOneSysStudentDept(Long id){
        return sysStudentDeptService.getOneSysStudentDept(id);
    }

    /**
     * 导出测试用01列表
     */
    @PreAuthorize("@ss.hasPermi('test0:test01:export')")
    @Log(title = "测试用01", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, SysStudentDept sysStudentDept)
    {
        List<SysStudentDept> list = sysStudentDeptService.selectSysStudentDeptList(sysStudentDept);
        ExcelUtil<SysStudentDept> util = new ExcelUtil<SysStudentDept>(SysStudentDept.class);
        util.exportExcel(response, list, "测试用01数据");
    }

    /**
     * 获取测试用01详细信息
     */
    @PreAuthorize("@ss.hasPermi('test0:test01:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(sysStudentDeptService.selectSysStudentDeptById(id));
    }

    /**
     * 新增测试用01
     */
    @PreAuthorize("@ss.hasPermi('test0:test01:add')")
    @Log(title = "测试用01", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody SysStudentDept sysStudentDept)
    {
        return toAjax(sysStudentDeptService.insertSysStudentDept(sysStudentDept));
    }

    /**
     * 修改测试用01
     */
    @PreAuthorize("@ss.hasPermi('test0:test01:edit')")
    @Log(title = "测试用01", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody SysStudentDept sysStudentDept)
    {
        return toAjax(sysStudentDeptService.updateSysStudentDept(sysStudentDept));
    }

    /**
     * 删除测试用01
     */
    @PreAuthorize("@ss.hasPermi('test0:test01:remove')")
    @Log(title = "测试用01", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(sysStudentDeptService.deleteSysStudentDeptByIds(ids));
    }
}
