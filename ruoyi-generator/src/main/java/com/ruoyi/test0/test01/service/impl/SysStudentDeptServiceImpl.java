package com.ruoyi.test0.test01.service.impl;

import java.util.List;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.conditions.query.LambdaQueryChainWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ruoyi.test0.test01.domain.SysStudentDeptVo;
import com.ruoyi.test0.test01.mapper.SysStudentMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Optional;

import com.ruoyi.common.utils.StringUtils;
import org.springframework.transaction.annotation.Transactional;
import com.ruoyi.test0.test01.domain.SysStudent;
import com.ruoyi.test0.test01.mapper.SysStudentDeptMapper;
import com.ruoyi.test0.test01.domain.SysStudentDept;
import com.ruoyi.test0.test01.service.ISysStudentDeptService;

/**
 * 测试用01Service业务层处理
 *
 * @author ruoyi
 * @date 2022-05-06
 */
@Service
public class SysStudentDeptServiceImpl extends ServiceImpl<SysStudentDeptMapper, SysStudentDept> implements ISysStudentDeptService {
    @Autowired
    private SysStudentDeptMapper sysStudentDeptMapper;

    @Autowired
    private SysStudentMapper sysStudentMapper;

    /**
     * 查询测试用01列表
     *
     * @param sysStudentDept 测试用01
     * @return 测试用01
     */
    @Override
    public List<SysStudentDept> selectSysStudentDeptList(SysStudentDept sysStudentDept) {

        List<SysStudentDept> sysStudentDepts = new LambdaQueryChainWrapper<>(sysStudentDeptMapper)
                .like(StringUtils.isNotBlank(sysStudentDept.getDeptName()),SysStudentDept::getDeptName,sysStudentDept.getDeptName())
                .list();
        return sysStudentDepts;
    }

    /**
     * 查询测试用01
     *
     * @param id 测试用01主键
     * @return 测试用01
     */
    @Override
    public SysStudentDept selectSysStudentDeptById(Long id) {
        return sysStudentDeptMapper.selectById(id);
    }


    public SysStudentDeptVo getOneSysStudentDept(Long id)
    {
        LambdaQueryWrapper<SysStudentDept> wrapper = Wrappers.lambdaQuery(SysStudentDept.class).eq(SysStudentDept::getId, id);
        SysStudentDeptVo sysStudentDeptVo = Optional.ofNullable(sysStudentDeptMapper.selectOne(wrapper)).map(SysStudentDeptVo::new).orElse(null);
        Optional.ofNullable(sysStudentDeptVo).ifPresent(this::addSysStudentInfo);
        return sysStudentDeptVo;
    }

    private void addSysStudentInfo(SysStudentDeptVo sysStudentDeptVo) {
        LambdaQueryWrapper<SysStudent> wrapper = Wrappers.lambdaQuery(SysStudent.class).eq(SysStudent::getDeptId, sysStudentDeptVo.getId());
        List<SysStudent> sysStudent = sysStudentMapper.selectList(wrapper);
        sysStudentDeptVo.setSysStudentList(sysStudent);
    }


    /**
     * 新增测试用01
     *
     * @param sysStudentDept 测试用01
     * @return 结果
     */
    @Transactional
    @Override
    public int insertSysStudentDept(SysStudentDept sysStudentDept) {
        int rows = sysStudentDeptMapper.insertSysStudentDept(sysStudentDept);
        insertSysStudent(sysStudentDept);
        return rows;
    }

    /**
     * 修改测试用01
     *
     * @param sysStudentDept 测试用01
     * @return 结果
     */
    @Transactional
    @Override
    public int updateSysStudentDept(SysStudentDept sysStudentDept) {
        sysStudentDeptMapper.deleteSysStudentByDeptId(sysStudentDept.getId());
        insertSysStudent(sysStudentDept);
        return sysStudentDeptMapper.updateSysStudentDept(sysStudentDept);
    }

    /**
     * 批量删除测试用01
     *
     * @param ids 需要删除的测试用01主键
     * @return 结果
     */
    @Transactional
    @Override
    public int deleteSysStudentDeptByIds(Long[] ids) {
        sysStudentDeptMapper.deleteSysStudentByDeptIds(ids);
        return sysStudentDeptMapper.deleteSysStudentDeptByIds(ids);
    }

    /**
     * 删除测试用01信息
     *
     * @param id 测试用01主键
     * @return 结果
     */
    @Transactional
    @Override
    public int deleteSysStudentDeptById(Long id) {
        sysStudentDeptMapper.deleteSysStudentByDeptId(id);
        return sysStudentDeptMapper.deleteSysStudentDeptById(id);
    }

    /**
     * 新增学生信息信息
     *
     * @param sysStudentDept 测试用01对象
     */
    public void insertSysStudent(SysStudentDept sysStudentDept) {
        List<SysStudent> sysStudentList = sysStudentDept.getSysStudentList();
        Long id = sysStudentDept.getId();
        if (StringUtils.isNotNull(sysStudentList)) {
            List<SysStudent> list = new ArrayList<SysStudent>();
            for (SysStudent sysStudent : sysStudentList) {
                sysStudent.setDeptId(id);
                list.add(sysStudent);
            }
            if (list.size() > 0) {
                sysStudentDeptMapper.batchSysStudent(list);
            }
        }
    }
}
