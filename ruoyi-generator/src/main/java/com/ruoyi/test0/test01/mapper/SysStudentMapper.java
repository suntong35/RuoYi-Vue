package com.ruoyi.test0.test01.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ruoyi.test0.test01.domain.SysStudent;
import com.ruoyi.test0.test01.domain.SysStudentDept;

import java.util.List;

/**
 * 测试用01Mapper接口
 * 
 * @author ruoyi
 * @date 2022-05-06
 */
public interface SysStudentMapper extends BaseMapper<SysStudent>
{
    /**
     * 查询测试用01列表
     *
     * @param sysStudentDept 测试用01
     * @return 测试用01集合
     */
    public List<SysStudentDept> selectSysStudentDeptList(SysStudentDept sysStudentDept);



    /**
     * 查询测试用01
     * 
     * @param id 测试用01主键
     * @return 测试用01
     */
    public SysStudentDept selectSysStudentDeptById(Long id);



    /**
     * 新增测试用01
     * 
     * @param sysStudentDept 测试用01
     * @return 结果
     */
    public int insertSysStudentDept(SysStudentDept sysStudentDept);

    /**
     * 修改测试用01
     * 
     * @param sysStudentDept 测试用01
     * @return 结果
     */
    public int updateSysStudentDept(SysStudentDept sysStudentDept);

    /**
     * 删除测试用01
     * 
     * @param id 测试用01主键
     * @return 结果
     */
    public int deleteSysStudentDeptById(Long id);

    /**
     * 批量删除测试用01
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteSysStudentDeptByIds(Long[] ids);

    /**
     * 批量删除学生信息
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteSysStudentByDeptIds(Long[] ids);
    
    /**
     * 批量新增学生信息
     * 
     * @param sysStudentList 学生信息列表
     * @return 结果
     */
    public int batchSysStudent(List<SysStudent> sysStudentList);
    

    /**
     * 通过测试用01主键删除学生信息信息
     * 
     * @param id 测试用01ID
     * @return 结果
     */
    public int deleteSysStudentByDeptId(Long id);
}
